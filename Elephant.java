public class Elephant {
	private String color;
	private int weight;
	private int height;
	private int money;
	private String choice;
	
	public Elephant(String color, int weight, int height, String choice){
		this.color = color;
		this.weight = weight;
		this.height = height;
		this.money = 100000;
		this.choice = choice;
	}
	
	public void eatingFood(){
		if(weight <= 10000){
			System.out.println("Feed your elephant pet more today");
		}else{
			System.out.println("Put your pet elephant on a diet right now please");
			
		}
	}
	public void rarity(){
		if(color.equals("pink")){
			System.out.println("Wow, your pet elephant is extremely rare");
		}else{
			System.out.println("Wow, your pet elephant is not rare at all");
		}
	}
	public void setColor(String color){
		this.color = color;
	}
	
	public String getColor(){
		return this.color;
	}
	
	public int getWeight(){
		return this.weight;
	}
	
	public int getHeight(){
		return this.height;
	}
	
	public int getMoney(){
		return this.money;
	}
	
	public String getChoice(){
		return this.choice;
	}
	
	public void calculateWorth(){
		if(choice.equals("yes") && color.equals("pink")){
			System.out.println("Since your pet is rare, it is extremely valuable.It is worth 1 million dollars");
			money = 1000000;
			System.out.println("Your balance is " + money);
		}else{
			System.out.println("Your pet is not rare, so it is only worth 20 thousand dollars");
			money = 20000;
			System.out.println("Your balance is " + money);
		}
	}
}
